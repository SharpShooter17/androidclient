package pl.dmcs.gpstracker.client

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import pl.dmcs.gpstracker.client.`package`.api
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun getConfiguration(view: View?) {
        Service.addInternetAccess(this)
        val intent = Intent(this, TrackerActivity::class.java)

        api.getConfiguration(configId.text.toString().toLong())
            .enqueue(object : Callback<Config> {
                override fun onFailure(call: Call<Config>?, t: Throwable?) {
                    toast("No response! Check your internet connection!")
                }

                override fun onResponse(call: Call<Config>?, response: Response<Config>?) {
                    if (response!!.code() != 200) {
                        toast("Not found configuration with given identifier!")
                    } else {
                        intent.putExtra("config", response.body())
                        startActivity(intent)
                    }
                }
            })

    }

    fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

}
