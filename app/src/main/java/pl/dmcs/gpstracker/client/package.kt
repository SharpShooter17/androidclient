package pl.dmcs.gpstracker.client

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object `package` {

    val api = RestAPI()

}

class RestAPI {

    private val api: Api

    init {
        val builder = Retrofit.Builder()
            .baseUrl("http://ujazdowski.net.pl")
            .addConverterFactory(MoshiConverterFactory.create())
        val retrofit = builder.build()
        api = retrofit.create(Api::class.java)
    }


    fun getConfiguration(id: Long): Call<Config> {
        return api.getConfigurationForId(id)
    }

    fun postLocation(localization: Localization): Call<Void> {
        return api.postLocation(localization)
    }
}
