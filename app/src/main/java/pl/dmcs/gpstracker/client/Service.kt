package pl.dmcs.gpstracker.client

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

object Service {

    fun addInternetAccess(activity: AppCompatActivity) {
        addPermission(activity, Manifest.permission.INTERNET)
    }

    fun addLocationAccess(activity: AppCompatActivity) {
        addPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
    }

    fun addActivityRecogitionAccess(activity: AppCompatActivity) {
        addPermission(activity, Manifest.permission.ACTIVITY_RECOGNITION)
    }

    private fun addPermission(activity: AppCompatActivity, permission: String) {
        if (!isPermissionGranted(activity, permission)) {
            ActivityCompat.requestPermissions(activity, arrayOf(permission), 101)
        }
    }

    private fun isPermissionGranted(context: Context, permission: String): Boolean {
        return ContextCompat.checkSelfPermission(context, permission)
            .equals(PackageManager.PERMISSION_GRANTED)
    }

}
