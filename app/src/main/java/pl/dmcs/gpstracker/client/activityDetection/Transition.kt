package pl.dmcs.gpstracker.client.activityDetection

data class Transition(
    val transitionName: String,
    val type: String
)
