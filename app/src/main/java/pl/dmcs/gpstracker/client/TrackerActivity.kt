package pl.dmcs.gpstracker.client

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.location.DetectedActivity
import pl.dmcs.gpstracker.client.activityDetection.BackgroundDetectedActivitiesService
import pl.dmcs.gpstracker.client.activityDetection.Transition
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class TrackerActivity : AppCompatActivity() {

    private lateinit var broadcastReceiver: BroadcastReceiver
    private lateinit var logsToDisplayAdapter: ArrayAdapter<String>
    private lateinit var logsToDisplay: MutableList<String>
    private lateinit var transitionName: String
    private lateinit var transitionList: MutableList<Transition>
    private lateinit var listView: ListView
    private lateinit var locationManager: LocationManager
    private lateinit var locationListener: LocationListener
    private lateinit var timer: Timer
    private lateinit var currentLocation: Location
    private lateinit var config : Config

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tracker)
        config = intent.getSerializableExtra("config") as Config
        init()
    }

    private fun init() {
        addPermitions()

        initClassFields()
        locationListenerInit()
        activityTrackingInit()
    }

    private fun initClassFields() {
        logsToDisplay = ArrayList()
        logsToDisplayAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, logsToDisplay)
        listView = findViewById(R.id.listView)
        listView.adapter = logsToDisplayAdapter;
        transitionList = ArrayList()
    }

    private fun activityTrackingInit() {
        broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (intent.action == BROADCAST_DETECTED_ACTIVITY) {
                    logsToDisplayAdapter.notifyDataSetChanged()
                    val type = intent.getIntExtra("type", -1)
                    val confidence = intent.getIntExtra("confidence", 0)
                    handleTransition(type, confidence)
                }
            }
        }
        startTracking()
    }

    private fun locationListenerInit() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationListener = initLocationListener()
        locationManager.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            200,
            0f,
            locationListener
        )
        initPeriodicalTask()

    }

    private fun initPeriodicalTask() {
        timer = Timer()
        val task: TimerTask = object : TimerTask() {
            override fun run() {
                if (::currentLocation.isInitialized) {
                    updateLocation()
                }
            }
        }
        val period: Long = config.posInterval * 1000L
        timer.schedule(task, 0, period)
    }

    private fun addPermitions() {
        Service.addInternetAccess(this)
        Service.addLocationAccess(this)
        Service.addActivityRecogitionAccess(this)
    }

    fun onClear(view: View?) {
        logsToDisplay.clear();
        logsToDisplayAdapter.notifyDataSetChanged()
    }

    fun onChangeConfig(view: View?) {
        stopTrakcing()
        stopTimer()
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    fun onStop(view: View?) {
        stopTrakcing()
        stopTimer()
    }

    private fun stopTimer() {
        timer.cancel()
        timer.purge()
    }


    private fun stopTrakcing() {
        val intent = Intent(this@TrackerActivity, BackgroundDetectedActivitiesService::class.java)
        stopService(intent)
    }


    override fun onResume() {
        super.onResume()
        registerReceiver()
    }

    private fun registerReceiver() {
        LocalBroadcastManager.getInstance(this).registerReceiver(
            broadcastReceiver,
            IntentFilter(BROADCAST_DETECTED_ACTIVITY)
        )
        logTextToView("Transitions registered successfully")
    }


    private fun startTracking() {
        logsToDisplay.add(
            currentDateTime() + " Started detected activities service"
        )
        val intent = Intent(this@TrackerActivity, BackgroundDetectedActivitiesService::class.java)
        startService(intent)
    }

    private fun currentDateTime(): String? {
        val date: Date = Calendar.getInstance().time
        val dateFormat = SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.ENGLISH)
        return dateFormat.format(date)
    }

    private fun handleTransition(type: Int, confidence: Int) {
        transitionName = activityToString(type)
        if (confidence > CONFIDENCE) {
            addTransitionToLog(transitionName)
        }
    }

    private fun addTransitionToLog(newTransitionName: String?) {
        if (transitionList.size > 0) {
            val (previousTransitionName) = transitionList[transitionList.size - 1]
            if (previousTransitionName != newTransitionName) {
                logTransition(previousTransitionName!!, "EXIT")
                logTransition(newTransitionName!!, "ENTER")
            }
        } else {
            logTransition(newTransitionName!!, "ENTER")
        }
    }

    private fun logTransition(transitionName: String, type: String) {
        transitionList.add(Transition(transitionName, type))
        logTextToView("Transition: $transitionName (${type})")
    }

    private fun logTextToView(text: String) {
        logsToDisplay.add(currentDateTime() + " " + text)
        logsToDisplayAdapter.notifyDataSetChanged()
    }


    fun activityToString(acitivityType: Int): String {
        when (acitivityType) {
            DetectedActivity.IN_VEHICLE -> {
                return "IN_VEHICLE"
            }
            DetectedActivity.ON_BICYCLE -> {
                return "ON_BICYCLE"
            }
            DetectedActivity.ON_FOOT -> {
                return "ON_FOOT"
            }
            DetectedActivity.RUNNING -> {
                return "RUNNING"
            }
            DetectedActivity.STILL -> {
                return "STILL"
            }
            DetectedActivity.TILTING -> {
                return "TILTING"
            }
            DetectedActivity.WALKING -> {
                return "WALKING"
            }
            DetectedActivity.UNKNOWN -> {
                return "UNKNOWN"
            }
        }
        return "UNKNOWN"
    }

    private fun initLocationListener(): LocationListener {
        return object : LocationListener {

            override fun onLocationChanged(location: Location?) {
                currentLocation = location!!
            }
            override fun onStatusChanged(s: String, i: Int, bundle: Bundle) {}

            override fun onProviderEnabled(s: String) {
            }
            override fun onProviderDisabled(s: String) {
            }
        }
    }

    private fun updateLocation() {
        val localization = Localization(config.objectIdentifier, currentLocation)
        `package`.api.postLocation(localization)
            .enqueue(object : Callback<Void> {
                override fun onFailure(call: Call<Void>, t: Throwable) {
                    logTextToView("Unsuccessful sending location")
                }

                override fun onResponse(call: Call<Void>, response: Response<Void>) {
                    logTextToView("Successful sending location")
                }

            })
    }

    companion object {

        val BROADCAST_DETECTED_ACTIVITY = "activity_intent"

        internal val DETECTION_INTERVAL_IN_MILLISECONDS: Long = 1000

        val CONFIDENCE = 70
    }
}
