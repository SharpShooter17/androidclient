package pl.dmcs.gpstracker.client

import java.io.Serializable

data class Config(
    val id: Long,
    val name: String,
    val token: String,
    val objectIdentifier: String,
    val posInterval: Int
) : Serializable