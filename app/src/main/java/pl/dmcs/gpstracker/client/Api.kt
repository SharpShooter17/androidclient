package pl.dmcs.gpstracker.client

import retrofit2.Call
import retrofit2.http.*

interface Api {

    @GET("/api/configs/{id}")
    fun getConfigurationForId(@Path("id") id: Long): Call<Config>

    @POST("/api/localizations")
    @Headers("Content-type: application/json")
    fun postLocation(@Body location: Localization): Call<Void>
}
