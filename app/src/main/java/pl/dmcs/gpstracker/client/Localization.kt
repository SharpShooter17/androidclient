package pl.dmcs.gpstracker.client

import android.location.Location
import java.lang.reflect.Constructor
import java.util.*

class Localization(
    var acc: Float,
    val alt: Double,
    val bea: Float,
    val lat: Double,
    val lon: Double,
    val prov: String,
    val spd: Float,
    val sat: Int,
    val datetime: Long,
    val serial: String,
    val tid: String,
    val plat: String,
    val platVersion: Int,
    val bat: Int
) {
    constructor(objectId: String, location: Location) : this(
        location.accuracy,
        location.altitude,
        0F,
        location.latitude,
        location.longitude,
        "GPS",
        0F,
        0,
        Calendar.getInstance().timeInMillis,
        "",
        objectId,
        "Android",
        1,
        1
    )


}
